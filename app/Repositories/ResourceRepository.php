<?php
/**
 * Created by PhpStorm.
 * User: arnaud
 * Date: 05/02/19
 * Time: 11:46
 */

namespace App\Repositories;


interface ResourceRepository
{
    public function getPaginate($n);

    public function store(Array $inputs);

    public function getById($id);

    public function update($id, Array $inputs);

    public function destroy($id);
}